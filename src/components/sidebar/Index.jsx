import { useEffect, useState } from 'react'
import { useIndex } from '../../context/context'
import { useIndexObserver } from '../../hooks/hooks'

export default function Index({ headings = [] }) {
	const [activeId, setActiveId] = useState()
	const { setHeadings } = useIndex()
	
	useIndexObserver(setActiveId)

	useEffect(() => {
		setHeadings(headings)
	}, [headings, setHeadings])

	return (
		<div aria-label='Table of contents' className='sticky top-3 overflow-auto' >
			<p className='text-2xl border-b border-slate-600 mb-3'>Tabla de Contenidos</p>
			<ul className='list-none list-inside'>
				{headings && headings?.map( heading => (
					<li
						key={heading.id} 
						className={heading.id === activeId 
							? 'px-1 py-2 bg-slate-200 dark:bg-[#253e41] border-l-4 border-l-cyan-800'
							: 'px-1 border-l-4 border-l-slate-800 my-1'
						}
					>
						<a href={`#${heading.id}`}
							className='hover:text-[#3473b2] dark:text-[#c9d1d9] dark:hover:text-white'
							onClick={e => {
								e.preventDefault()
								document.querySelector(`#${heading.id}`).scrollIntoView({ behavior: 'smooth' })
							}}
						>{heading.title}</a>

						{heading.items.length > 0 && (
							<ul>
								{heading.items.map(child => (
									<li 
										key={child.id} 
										className={child.id === activeId 
											? 'px-1 py-2 bg-[#253e41] border-l-4 border-l-cyan-800' 
											: 'px-1 border-l-4 border-l-slate-800 my-1'
										}
									>
										<a href={`#${child.id}`}
											className='hover:text-[#3473b2] dark:text-[#c9d1d9] dark:hover:text-white'
											onClick={e => {
												e.preventDefault()
												document.querySelector(`#${child.id}`).scrollIntoView({ behavior: 'smooth' })
											}}
										>{child.title}</a>
									</li>
								))}
							</ul>
						)}
					</li>
				))}
			</ul>
		</div>
	)
}
