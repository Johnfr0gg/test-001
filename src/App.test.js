import '@testing-library/jest-dom'
import { render, screen } from "@testing-library/react"
import Home from './components/home/Home'

global.ResizeObserver = jest.fn().mockImplementation(() => ({
	observe: jest.fn(),
	unobserve: jest.fn(),
	disconnect: jest.fn(),
}))

test("renders learn react link", () => {
	render(<Home />)
	// const linkElement = screen.queryByText(/Hello World!/i)
	expect( screen.getByText('Hello World!') ).toBeInTheDocument()
});
