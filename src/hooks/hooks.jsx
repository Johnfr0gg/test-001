import { useEffect, useRef, useState } from "react"
import { useIndex } from "../context/context"
import { filterByMdFiles } from "../lib/utils"

const BASE_URL = 'https://gitlab.com/api/v4/projects'
const REPO_ID = '50695063'
const REF = 'pages'
const FETCH_TREE = 'repository/tree'
const FETCH_FILES = 'repository/files'
const PATH_POSTS = 'publicaciones'

const log = (arr = []) => arr.length > 0 ? arr[0] : {}

export function useGetMarkdown({ fileName, setMarkdown, setLoading, path }) {
	const ref = REF ? `ref=${REF}` : ''

	useEffect(() => {
		const url = `${BASE_URL}/${REPO_ID}/${FETCH_FILES}/${encodeURIComponent(path? path + '/' + fileName : fileName)}/raw?${ref ? ref : ''}`
		fetch(url)
			.then(res => res.text())
			.then(res => {
				setMarkdown(res)
				setLoading(false)
			})
			.catch(error => console.warn(error))
	}, [fileName, ref, setMarkdown, setLoading, path])
}

export function useListPublications({ setFileList, path = PATH_POSTS }) {
	const [error, setError] = useState(false)

	useEffect(() => {
		const getData = async () => {
			try{
				const response = await fetch(`${BASE_URL}/${REPO_ID}/${FETCH_TREE}?id=${REPO_ID}&ref=${REF}&path=${path}&recursive=false&per_page=100`)
				let files = await response.json()
				let filePaths = files.map(item => item.path)
				let fileCommitHistories = await Promise.all(
					filePaths.map(async path => {
						let res = await fetch(`${BASE_URL}/${REPO_ID}/${FETCH_FILES}/${encodeURIComponent(path)}/blame?ref=${REF}`)
						return await res.json()
					})
				)
				let publications = filterByMdFiles(
					files.map((item, i) => ({
						name: item.name,
						path: item.path,
						date: log(fileCommitHistories[i])?.commit?.authored_date || 'no date',
						author: log(fileCommitHistories[i])?.commit?.author_name || 'no author',
						lines: log(fileCommitHistories[i])?.lines?.slice(0, 20) || []
					}))
				)
				setFileList(publications)
				return null
			}
			catch(e){
				console.error('Error', e)
				setError(true)
				return error
			}
		}
		getData()
	}, [error, setFileList, path])

	return error
}

export function useIndexObserver(setActiveId) {
	const { headings } = useIndex()
	const headingElementsRef = useRef({})
	
	useEffect(() => {
		const headingElements = Array.from( document.querySelectorAll('h2, h3, h4') )

		const callback = (headings) => {
			headingElementsRef.current = headings.reduce( (map, headingElement) => {
				map[headingElement.target.id] = headingElement
				return map
			}, headingElementsRef.current)

			const visibleHeadings = []
			Object.keys(headingElementsRef.current).forEach( key => {
				const headingElement = headingElementsRef.current[key]
				if (headingElement.isIntersecting) visibleHeadings.push(headingElement)
			})
	
			const getIndexFromId = (id) => headingElements.findIndex( heading => heading.id === id )

			if(visibleHeadings.length === 1) setActiveId(visibleHeadings[0].target.id)
			else if(visibleHeadings.length > 1) {
				const sortedVisibleHeadings = visibleHeadings.sort(
					(a,b) => getIndexFromId(a.target.id) > getIndexFromId(b.target.id)
				)
				setActiveId(sortedVisibleHeadings[0].target.id)
			}
		}

		const observer = new IntersectionObserver(callback, {
			rootMargin: '0px 0px -40% 0px',
		})

		headingElements.shift()
		headingElements.forEach( item => observer.observe(item) )

		return () => observer.disconnect()
	}, [headings, setActiveId])
}

export function useResize(refObject, breakpoints) {
	const firstQuery = Object.keys(breakpoints[0])[0]
	const [breakSize, setBreakSize] = useState(firstQuery)

	const observer = useRef(
		new ResizeObserver( entries => {
			const width = entries[0].contentRect.width
			setBreakSize(findBreakPoint(breakpoints, width))
		})
	)

	useEffect(() => {
		const watcher = observer?.current
		const target = refObject?.current
		if(!target) return
		watcher.observe(target)

		return () => watcher.unobserve(target)
		
	}, [refObject, observer])

	return breakSize
}

function findBreakPoint(breakpoints, width) {
	const breakpointIndex = breakpoints
		.map(x => Object.values(x)[0])
		.findIndex(x => width < x);

	if (breakpointIndex === -1) {
		return Object.keys(breakpoints[breakpoints.length - 1])[0];
	}

	return Object.keys(breakpoints[breakpointIndex])[0];
}
