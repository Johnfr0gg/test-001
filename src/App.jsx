import { BrowserRouter, Routes, Route } from 'react-router-dom'
import Home from './components/home/Home'
import Layout from './components/Layout'
import MdPost from './components/mdPost/MdPost'
import { IndexProvider } from './context/context'
import { useState } from 'react'
import Events from './components/posts/Events'
import { useListPublications } from './hooks/hooks'
import EnlacesExternos from './components/home/EnlacesExternos'
import AcercaDe from './components/home/AcercaDe'

function App() {
	const [eventList, setEvents] = useState([])
	const [publications, setPublications] = useState([])

	useListPublications({ setFileList: setPublications })
	useListPublications({ setFileList: setEvents, path: 'eventos' })

	return (
		<IndexProvider>
		<BrowserRouter basename={process.env.PUBLIC_URL}>
		<Routes>
			<Route path='/' element={<Layout />}>
				<Route index element={<Home events={eventList} publications={publications} />} />
				<Route path='/publicaciones'>
					<Route
						path=':fileName'
						element={<MdPost currentDir={'publicaciones'} events={eventList} />}
					/>
				</Route>
				<Route path='/eventos' element={<Events events={eventList} />}/>
				<Route
					path='/eventos/:fileName'
					element={<MdPost currentDir={'eventos'} events={eventList} />}
				/>
				<Route path='/enlaces-externos' element={<EnlacesExternos events={eventList} />}/>
				<Route path='/acerca' element={<AcercaDe events={eventList} />} />
			</Route>
		</Routes>
		</BrowserRouter>
		</IndexProvider>
	);
}

export default App;
